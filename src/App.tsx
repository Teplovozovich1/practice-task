import React, { FormEvent, useCallback, useEffect } from "react"
import './App.css'
import { InputField, useInput } from "./components/Common/InputField/InputField"
import { useDispatch, useSelector } from "react-redux"
import { decrement, increment, searchDocument, searchDocumentTest } from "./redux/reducers/counterSlice"
import { Posts } from "./components/Posts/Posts"
import { AppDispatch } from "./redux/store"
import DocumentInformation from "./components/DocumentInformation/DocumentInformation"

const App: React.FC = () => {
  const [buttonDisabled, setButtonDisabled] = React.useState(true)
  const addressInput = useInput('', 5, 100)
  const lSdInput = useInput('', 10, 25)
  const fioInput = useInput('', 3, 100)
  const kadastrovyNumberInput = useInput('', 5, 30)
  const documentInput = useInput('', 5, 30)
  const snilsInput = useInput('', 14, 14)
  const dispatch = useDispatch<AppDispatch>();
  const [canDispatchForm, setCanDispatchForm] = React.useState(false);
  const documentData = useSelector((state: any) => state.counter.documentData)

  const allFieldValues = [lSdInput.value, addressInput.value, kadastrovyNumberInput.value, fioInput.value, documentInput.value]
  const allFieldErrors = [lSdInput.error, addressInput.error, kadastrovyNumberInput.error, fioInput.error, documentInput.error]

  useEffect(() => {
    let isAnyFieldFilled = allFieldValues.some(value => value.trim() !== '');
    if (isAnyFieldFilled) {
      if (canDispatchForm === true) {
        setCanDispatchFormCallback(allFieldValues);
      }
    }
    setButtonDisabled(
      !lSdInput.value &&
      !addressInput.value &&
      !fioInput.value &&
      !kadastrovyNumberInput.value &&
      !documentInput.value &&
      !snilsInput.value
    );
  }, [allFieldValues]);

  const setCanDispatchFormCallback = useCallback((allFieldValues: string[]) => {
    const anyError = allFieldErrors.some(error => error);
    if (!anyError) {
      dispatch(searchDocument(allFieldValues))
    }

    setCanDispatchForm(false);
  }, allFieldErrors);


  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()

    if (lSdInput.value) { validateInput(lSdInput) }
    if (addressInput.value) { validateInput(addressInput) }
    if (fioInput.value) { validateInput(fioInput) }
    if (kadastrovyNumberInput.value) { validateInput(kadastrovyNumberInput) }
    if (documentInput.value) { validateInput(documentInput) }
    if (snilsInput.value) { validateInput(snilsInput) }

    setCanDispatchForm(true)
  }


  const validateInput = (input: {
    value: string;
    setError: (value: boolean) => void;
    setErrorMessage: (value: string) => void;
    minLength?: number;
    maxLength?: number
  }) => {
    const forbiddenCharacters = ['!', '@', '#', '$', '%'];

    if (!input.value.trim()) {
      input.setError(true);
      input.setErrorMessage("Поле не может быть пустым");
    } else if (forbiddenCharacters.some(char => input.value.includes(char))) {
      input.setError(true);
      input.setErrorMessage("Недопустимые символы");
    } else if ((input.minLength && input.value.length < input.minLength)) {
      input.setError(true);
      input.setErrorMessage(`Длина должна быть больше ${input.minLength}`);
    } else if (input.maxLength && input.value.length > input.maxLength) {
      input.setError(true);
      input.setErrorMessage(`Символов не может быть больше ${input.maxLength}`);
    } else {
      input.setError(false);
    }
  };

  return (
    <div className="page-center">
      {/* <Posts/> */}
      <div className="main_container">
        <div className="form_container">
          <form onSubmit={handleSubmit}>
            <h3 className="form-title">Form</h3>
            <InputField
              label="Лицевой счет (Аккаунт?)"
              mask="9999 99999 99999 9999"
              maskChar=""
              type="text"
              name="password"
              placeholder="0506 372737 37538 3934"
              {...lSdInput}
            />
            <InputField
              label="Адрес"
              mask=""
              type="text"
              name="address"
              placeholder="г. Тюмень, ул. Профсоюзная, д. 88, кв. 19"
              {...addressInput}
            />
            <InputField
              label="Кадастровый номер"
              mask="99 99 9999999 9999"
              maskChar=""
              type="text"
              name="kadastrovyNumber"
              placeholder="72 03 1905839 1234"
              {...kadastrovyNumberInput}
            />
            <InputField
              label="ФИО"
              mask=""
              type="text"
              name="fio"
              placeholder="Иванов Иван Иванович"
              {...fioInput}
            />
            <InputField
              label="Докумет (Паспорт)"
              mask=""
              type="text"
              name="document"
              placeholder="Серия номер"
              {...documentInput}
            />
            {/* <InputField
              label="СНИЛС"
              mask="999 999 999 99"
              maskChar=""
              type="text"
              name="snils"
              placeholder="732 123 431 01"
              {...snilsInput}
            /> */}
            <button disabled={buttonDisabled} className="submit-btn">Отправить</button>
          </form>
        </div>
        <div className="">
          {
            !documentData ? null :
              documentData.map((record: Array<any>) => (
                <DocumentInformation documentData={record} />
              ))
          }
        </div>
      </div>
    </div>
  )
}
export default App;
