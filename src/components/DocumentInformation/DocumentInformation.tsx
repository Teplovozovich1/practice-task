import React from 'react';
import './DocumentInformation.scss'

interface DocumentInformationProps {
    documentData: any;
}

interface Owner {
    first_name: string;
    last_name: string;
    birth_date: string;
    snils: number;
    reg: string;
    reg_at: string;
}

const DocumentInformation: React.FC<DocumentInformationProps> = ({ documentData }) => {
    return (
        <div>

            <details className="details">
                <summary className="details__title">{documentData.address} <br></br>{documentData.kadastr}</summary>
                <div className="details__content">
                <p>{documentData.category}</p>
                    {
                        documentData.owners.map((owner: Owner, index: number) => (
                            <details key={index} className="details">
                                <summary className="details__title">{owner.first_name} {owner.last_name}</summary>
                                <div className="details__content">
                                    <p>Дата рождения: {owner.birth_date}</p>
                                    <p>Снилс: {owner.snils}</p>
                                    <p>Reg: {owner.reg}</p>
                                    <p>Зарегистрирован: {owner.reg_at}</p>
                                </div>
                            </details>
                        ))
                    }
                </div>
                <div className="details__content">
                    <details className="details">
                        <summary className="details__title">Файлы</summary>
                        <div className="details__content">
                            {
                                documentData.files.map((file: any, index: any) => (
                                    <div key={index}>
                                        <p>Место: {file.place}</p>
                                        <p>Библиотека: {file.library}</p>
                                        <p>Папка: {file.folder}</p>
                                        <p>Имя файла: {file.name}</p>
                                    </div>
                                ))
                            }
                            <p>Создано: {documentData.created_at}</p>
                        </div>
                    </details>
                    <p>{documentData.created_at}</p>
                    <p>{documentData.updated_at}</p>
                    <p>{documentData.category}</p>
                    <p>{documentData.document}</p>
                    <p>{documentData.document_at}</p>
                    <p>{documentData.area}</p>
                    <p>{documentData.oid}</p>
                </div>
            </details>
        </div>
    );
}

export default DocumentInformation;