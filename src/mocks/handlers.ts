import { HttpResponse, http } from "msw";
import { faker } from '@faker-js/faker';

const generateDocument = () => ({
    address: faker.location.streetAddress(),
    account: faker.number.int(),
    area: faker.number.int().toString(),
    category: faker.lorem.word(),
    created_at: faker.date.past().toISOString(),
    document: faker.lorem.word(),
    kadastr: faker.number.int(),
    owners: [
        {
            birth_date: faker.date.past().toISOString(),
            document: {
                issued_at: faker.date.past().toISOString(),
                name: faker.lorem.word(),
                number: faker.string.alphanumeric(10),
                organization: faker.company.name(),
                series: faker.string.alphanumeric(4),
            },
            first_name: faker.person.firstName(),
            last_name: faker.person.lastName(),
            patronymic_name: faker.person.middleName(),
            reg: faker.location.city(),
            reg_at: faker.date.past().toISOString(),
            share: faker.number.int().toString(),
            snils: faker.string.alphanumeric(11),
        },
        {
            inn: faker.number.int(),
            name: faker.person.firstName(),
            ogrn: faker.number.int(),
            reg: faker.location.city(),
            reg_at: faker.date.past().toISOString(),
            share: faker.company.name(),
        },
    ],
    path: faker.system.filePath(),
});

const recordsDocument = {
    "records": [
        {
            "category": "\u0416\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [
                {
                    "first_name": "\u0421\u0435\u0440\u0433\u0435\u0439",
                    "last_name": "\u0414\u044e\u0440\u044f\u0433\u0438\u043d",
                    "patronymic_name": "\u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440\u043e\u0432\u0438\u0447",
                    "birth_date": "1980-10-13",
                    "snils": "058-155- 625 73",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "6503",
                        "number": "931300",
                        "issued_at": "2003-03-27"
                    },
                    "share": "1\/2",
                    "reg": "72-72\/005-72\/005\/012\/2016-285\/1",
                    "reg_at": "2016-02-25T08:49:03"
                },
                {
                    "first_name": "\u0411\u043e\u0440\u0438\u0441",
                    "last_name": "\u0414\u044e\u0440\u044f\u0433\u0438\u043d",
                    "patronymic_name": "\u0410\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440\u043e\u0432\u0438\u0447",
                    "birth_date": "1978-01-17",
                    "snils": "046-646-207 66",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "6511",
                        "number": "350958",
                        "issued_at": "2012-03-28"
                    },
                    "share": "1\/2",
                    "reg": "72-72\/005-72\/005\/012\/2016-284\/1",
                    "reg_at": "2016-02-25T08:49:03"
                }
            ],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20191219",
                    "name": "238.pdf"
                }
            ],
            "created_at": "2024-04-08T13:03:23",
            "updated_at": "2024-04-08T13:03:23",
            "address": "\u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0413\u043e\u043b\u044b\u0448\u043c\u0430\u043d\u043e\u0432\u0441\u043a\u0438\u0439 \u0440\u0430\u0439\u043e\u043d, \u0441. \u041c\u0430\u043b\u044b\u0448\u0435\u043d\u043a\u0430, \u0443\u043b. 50 \u041b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434\u043e\u043c 14, \u043a\u0432\u0430\u0440\u0442\u0438\u0440\u0430 6",
            "kadastr": "72:07:1001001:987",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-29268760",
            "document_at": "2019-12-04",
            "area": "38.40",
            "oid": "6613eb1db8b5f23448d8fd43"
        },
        {
            "category": "\u041d\u0435\u0436\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [
                {
                    "first_name": "\u041d\u0430\u0434\u0435\u0436\u0434\u0430",
                    "last_name": "\u041d\u0435\u0447\u0430\u0435\u0432\u0430",
                    "patronymic_name": "\u041c\u0438\u0445\u0430\u0439\u043b\u043e\u0432\u043d\u0430",
                    "birth_date": "1958-09-26",
                    "snils": "061-869-271 89",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "7104",
                        "number": "039357",
                        "issued_at": "2003-11-04"
                    },
                    "share": "1",
                    "reg": "72:23:0218001:6346-72\/001\/2017-2",
                    "reg_at": "2017-12-08T16:34:44"
                }
            ],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20200128\/1",
                    "name": "105.pdf"
                }
            ],
            "created_at": "2024-04-08T13:19:12",
            "updated_at": "2024-04-08T13:19:12",
            "address": "\u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0433. \u0422\u044e\u043c\u0435\u043d\u044c, \u0443\u043b. 50 \u043b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434. 30\/4",
            "kadastr": "72:23:0218001:6346",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-30142185",
            "document_at": "2019-12-16",
            "area": "103.40",
            "oid": "6613eed160ead896533e674b"
        },
        {
            "category": "\u0416\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20200128\/1",
                    "name": "107.pdf"
                }
            ],
            "created_at": "2024-04-08T13:19:43",
            "updated_at": "2024-04-08T13:19:43",
            "address": "\u043e\u0431\u043b. \u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f, \u0433. \u0422\u044e\u043c\u0435\u043d\u044c, \u0443\u043b. 50 \u043b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434. 46, \u043a\u0432. 140",
            "kadastr": "72:23:0218001:4287",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-30141456",
            "document_at": "2019-12-13",
            "area": "54.20",
            "oid": "6613eeef60ead896533e6761"
        },
        {
            "category": "\u041d\u0435\u0436\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [
                {
                    "first_name": "\u041e\u043b\u0435\u0441\u044f",
                    "last_name": "\u0420\u0435\u043c\u0435\u0437",
                    "patronymic_name": "\u0410\u043d\u0430\u0442\u043e\u043b\u044c\u0435\u0432\u043d\u0430",
                    "birth_date": "1980-12-02",
                    "snils": "069-665-304 08",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "2504",
                        "number": "305568",
                        "issued_at": "2004-06-11"
                    },
                    "share": "1",
                    "reg": "72-72-01\/246\/2011-257",
                    "reg_at": "2011-06-28T00:00:00"
                }
            ],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20200128\/1",
                    "name": "1108.pdf"
                }
            ],
            "created_at": "2024-04-08T13:20:45",
            "updated_at": "2024-04-08T13:20:45",
            "address": "\u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0433. \u0422\u044e\u043c\u0435\u043d\u044c, \u0443\u043b. 50 \u043b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434. 30, \u043f\u043e\u043c. \/2\u0431",
            "kadastr": "72:23:0218001:15265",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-30142061",
            "document_at": "2019-12-16",
            "area": "57.90",
            "oid": "6613ef2e60ead896533e678c"
        },
        {
            "category": "\u041d\u0435\u0436\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [
                {
                    "first_name": "\u0412\u0430\u043b\u0435\u043d\u0442\u0438\u043d",
                    "last_name": "\u0410\u043d\u0438\u0441\u0438\u043d",
                    "patronymic_name": "\u041f\u0430\u0432\u043b\u043e\u0432\u0438\u0447",
                    "birth_date": "1941-03-08",
                    "snils": "059-570-705 93",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "7100",
                        "number": "258578",
                        "issued_at": "2001-04-11"
                    },
                    "share": "1",
                    "reg": "72-01\/01-73\/2001-262",
                    "reg_at": "2002-01-15T00:00:00"
                }
            ],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20200128\/1",
                    "name": "1113.pdf"
                }
            ],
            "created_at": "2024-04-08T13:20:55",
            "updated_at": "2024-04-08T13:20:55",
            "address": "\u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0433. \u0422\u044e\u043c\u0435\u043d\u044c, \u0443\u043b. 50 \u043b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434. 30, \u043f\u043e\u043c. \/7",
            "kadastr": "72:23:0218001:6152",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-30141983",
            "document_at": "2019-12-16",
            "area": "85.30",
            "oid": "6613ef3860ead896533e6792"
        },
        {
            "category": "\u0416\u0438\u043b\u043e\u0435 \u043f\u043e\u043c\u0435\u0449\u0435\u043d\u0438\u0435",
            "owners": [
                {
                    "first_name": "\u041d\u0438\u043a\u043e\u043b\u0430\u0439",
                    "last_name": "\u0413\u0430\u0432\u0440\u0438\u043b\u043e\u0432",
                    "patronymic_name": "\u0410\u043d\u0430\u0442\u043e\u043b\u044c\u0435\u0432\u0438\u0447",
                    "birth_date": "1984-12-30",
                    "snils": "097-165-782 14",
                    "document": {
                        "name": "\u041f\u0430\u0441\u043f\u043e\u0440\u0442 \u0433\u0440\u0430\u0436\u0434\u0430\u043d\u0438\u043d\u0430 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438",
                        "series": "7104",
                        "number": "255133",
                        "issued_at": "2005-02-03"
                    },
                    "share": "1",
                    "reg": "72-72-01\/320\/2008-165",
                    "reg_at": "2008-11-21T00:00:00"
                }
            ],
            "files": [
                {
                    "place": "seafile",
                    "library": "45282a74-9e67-4ac1-b181-13efeda761c7",
                    "folder": "20200128\/1",
                    "name": "1138.pdf"
                }
            ],
            "created_at": "2024-04-08T13:21:34",
            "updated_at": "2024-04-08T13:21:34",
            "address": "\u0422\u044e\u043c\u0435\u043d\u0441\u043a\u0430\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u044c, \u0433. \u0422\u044e\u043c\u0435\u043d\u044c, \u0443\u043b. 50 \u043b\u0435\u0442 \u041e\u043a\u0442\u044f\u0431\u0440\u044f, \u0434. 74, \u043a\u043e\u0440\u043f. 2, \u043a\u0432. 58",
            "kadastr": "72:23:0218006:10873",
            "document": "\u041a\u0423\u0412\u0418-001\/2019-30141456",
            "document_at": "2019-12-13",
            "area": "64.50",
            "oid": "6613ef5f60ead896533e67ad"
        },
    ]
}


export const handlers = [
    http.get('/api/users', (resolver) => {
        return HttpResponse.json([
            {
                id: 1,
                name: "aboba"
            }
        ])
    }),
    http.post('api/messages', async ({ request }) => {
        const requestBody: any = await request.json()
        return HttpResponse.json({})
    }),
    http.post('api/testsearch', async ({ request }) => {
        const requestBody = await request.json()
        const document = generateDocument();
        return HttpResponse.json(document, { status: 201 })
    }),
    http.post('api/search', async ({ request }) => {
        const requestBody = await request.json()
        const document = recordsDocument;
        return HttpResponse.json(document, { status: 201 })
    }),
];
