import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { documentAPI } from '../../api/documentApi';

const initialState = {
  value: 0,
  status: 'idle',
  documentData: null,
};

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state, action) => {
      state.value -= 1;
      alert(action.payload)
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
    setDocumentData: (state, action) => {
      state.documentData = action.payload
    }
  }
});

export const { increment, decrement, incrementByAmount, setDocumentData } = counterSlice.actions;

export const searchDocumentTest = (amount: any) => async (dispatch: any, getState: any) => {
  let response = await documentAPI.search(amount)
  dispatch(decrement(response.data));
};
export const searchDocument = (data: string[]) => async (dispatch: any) => {
  const body = {
    account: data[0],
    address: data[1],
    kadastr: data[2],
    fio: data[3],
    passport: data[4]
  };
  let response = await documentAPI.searchDocument(body)
  console.log(response.records);
  
  dispatch(setDocumentData(response.records));
};


export default counterSlice.reducer;
